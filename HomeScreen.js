import React from "react";
import { StatusBar } from "react-native";
import ListArtist from'./TopArtist';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content,Text, Card, CardItem } from "native-base";
export default class HomeScreen extends React.Component {
  constructor(props){
    super(props);
    this.state={
    }
    this.myfunction=this.myfunction.bind(this)
  }
  myfunction(){
    alert(JSON.stringify(this.props))
  }
  render() {
    return (
      
      <Container>
          { /**Header*/ }
        <Header>
          <Left/>
          <Body>
            <Title>VALID EXAM</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
            <ListArtist
             artist={this.props.topArtists}/>
        </Content>

      </Container>
    );
  }
}