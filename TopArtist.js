
import React, { Component } from 'react';
import { Image, Linking } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
export default function ListArtist(props) {

    const artistList = props.artist
    const currencies = Object.keys(artistList);
    return currencies.map(currency => (
        <Card>

            <CardItem>
                <Left>

                    <Thumbnail source={{ uri: 'https://lastfm.freetls.fastly.net/i/u/34s/2a96cbd8b46e442fc41c2b86b821562f.png' }} />
                    <Body>
                        <Text>{artistList[currency].name}</Text>
                        <Text note>{artistList[currency].mbid}</Text>
                    </Body>
                </Left>
            </CardItem>
            <CardItem>
                <Left>
                    <Button transparent>
                        <Icon active name="play" />
                        <Text>{artistList[currency].listeners} Reproducciones</Text>
                    </Button>
                </Left>

                <Right>
                    <Button iconLeft onPress={() => {
                         Linking.openURL(artistList[currency].url);
                    }} >
                        <Icon active name="redo" />
                        <Text>SEE ON LAST FM</Text>
                    </Button>
                </Right>
            </CardItem>
        </Card>

    )
    )

}