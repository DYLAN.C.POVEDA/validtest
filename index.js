/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import HomeScreen from "./HomeScreen.js";
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
