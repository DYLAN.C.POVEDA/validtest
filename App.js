/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import HomeScreen from "./HomeScreen.js";

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      topArtists: ''
    }

  }

  componentDidMount() {
    fetch('http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=spain&api_key=829751643419a7128b7ada50de590067&format=json')
      .then(function (response) {
        return response.json();
      })
      .then(myJson => {
        this.setState({ topArtists: myJson.topartists.artist })
      })
  }
  render() {

    return (
      <HomeScreen
       topArtists={this.state.topArtists}/>
    );
  }
}


export default App;
